<?php
require "FormulaireBootstrap.php";
$form = new FormulaireBoostrap("/",$p_method='POST');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Formulaire php</title>
</head>
<body>
    <div class="container">
        <div class="content col-md-6 offset-md-3">
            <h1 class="title text-center">Inscription</h1>
            <?php
                
                // FIRSTNAME INPUT
                $input = $form->createInput( 'firstname', 'text',[ 'id'=>'firstname',
                        'class'=>'form-control',
                        'placeholder'=> "Enter your firstname",
                        'required' => 'required' 
                    ]
                );
                
                $form->addChildren($form->getForm(), $input);
                
                // LASTNAME INPUT
                $input = $form->createInput( 'lastname', 'text',[ 'id'=>'lastname',
                        'class'=>'form-control',
                        'placeholder'=> "Enter your lastname",
                        'required' => 'required' 
                    ]
                );
                $form->addChildren($form->getForm(), $input);

                // EMAIL INPUT
                $input = $form->createInput( 'email', 'email',[ 'id'=>'email',
                        'class'=>'form-control',
                        'placeholder'=> "Enter your email adress",
                        'required' => 'required' 
                    ]
                );
                $form->addChildren($form->getForm(), $input);

                // PASSWORD INPUT
                $input = $form->createInput( 'password', 'password',[ 'id'=>'password',
                        'class'=>'form-control',
                        'placeholder'=> "Enter your password",
                        'required' => 'required' 
                    ]
                );
                $form->addChildren($form->getForm(), $input);

                // PASSWORD CONFIRM INPUT
                $input = $form->createInput( 'password_confirm', 'password',[ 'id'=>'password_confirm',
                        'class'=>'form-control',
                        'placeholder'=> "Confirm your password",
                        'required' => 'required' 
                    ]
                );
                $form->addChildren($form->getForm(), $input);

                // BIOGRAPH TEXTARE
                $input = $form->createTextarea( 'biograph', [ 'id'=>'biograph',
                        'class'=>'form-control',
                        'placeholder'=> "Introduce yourself...",
                        'required' => 'required',
                        "rows" => 10,
                        "cols" => 30,
                    ]
                );
                $form->addChildren($form->form, $input);

                // SUBMIT BUTTOn
                $input = $form->createInput( 'inscription', 'submit',[ 'class'=>'btn btn-success btn-block',
                        "value" => "Inscription"
                    ]
                );
                $form->addChildren($form->getForm(), $input);
                
                echo $form->getForm();
            ?>
        </div>
    </div>
   

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>