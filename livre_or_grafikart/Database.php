<?php

class Database
{   
    private static $bdd;
    private $message;
    
    public function __construct( $p_message )
    {
        self::getBDD();
        $this->createMessageTable();
        $this->message = $p_message;
    }

    /**
    * Méthode qui crée l'unique instance de la variable $bdd
    * si elle n'existe pas encore puis la retourne.
    *
    * @param void
    * @return Singleton $bdd
    */
    public static function getBDD() {
    
        if(is_null(self::$bdd)) 
        {
            try{
                self::$bdd = new PDO('sqlite:'.__DIR__.'/db/Database.db');
                self::$bdd->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
                self::$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
            } catch(Exception $e) {
                echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
                exit();
            }
            //self::$bdd = new SQLite3();  
        }
        return self::$bdd;
    }

    /*
        Create a new table by giving a sql request
    */
    public static function createTable($sql)
    {
        try
        {
            $self::$bdd->exec($sql);
        }
        catch( Exception  $e)
        {
            echo "Impossible de créer la table ! ";
            echo $e->getMessage();
            exit();
        }
    }

    /*
        Create an article table
    */
    private function createMessageTable()
    {
        try
        {
            $sql = "CREATE TABLE IF NOT EXISTS messages(
                id integer primary key,
                pseudo varchar(100),
                msg TEXT NOT NULL,
                tohtml TEXT,
                tojson TEXT,
                created_at varchar(50),
                UNIQUE(msg) ON CONFLICT IGNORE );";
            
            self::$bdd->exec($sql);
        }
        catch(Exception $e)
        {
            echo "Impossible de créer la table article !";
            echo $e->getMessage();
            exit();
        }
    }

    /*
        Insert an article
    */
    public function saveMessage()
    {
        //try
        //{
            $sql = 'INSERT INTO messages(pseudo, msg, tohtml, tojson, created_at) 
                    VALUES(:pseudo, :msg, :tohtml, :tojson, :created_at);';
            $stmt = self::$bdd->prepare($sql);
            
            $pseudo     = $this->message->getPseudo();
            $message    =  $this->message->getMsg();
            $html       =  $this->message->toHTML();
            $json       = $this->message->toJSON();
            $created_at = $this->message->getCreated();
            $stmt->bindParam(':pseudo', $pseudo );
            $stmt->bindParam(':msg', $message );
            $stmt->bindParam(':tohtml', $html );
            $stmt->bindParam(':tojson', $json );
            $stmt->bindParam( ':created_at', $created_at );
            $result = $stmt->execute();
            return $result;
        
    }

    /*
        Get all articles
    */
    public static function getMessages()
    {
        try
        {
            $results = [];
            $sql = 'SELECT * FROM messages ';
    
            $query = self::$bdd->query($sql);
            
            foreach ($query->fetchAll() as $key => $obj) {
                array_push($results, $obj);
            }
            return $results;
        }
        catch(Exception $e)
        {}
    }


 




}

