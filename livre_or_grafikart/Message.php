<?php
require "functions.php";
class Message
{
    private $username;
    private $message;
    private $created;

    private $errors;

    public function __construct($p_username, $p_message )
    {
        $this->username = $p_username;
        $this->message  = preg_replace("%\n%",'<br>', $p_message);
        $this->created  = (new Datetime())->format("Y-m-d H:i:s");
    }

    // GETTERS
    public function getPseudo()
    {
        return $this->username;
    }
    public function getMsg()
    {
        return $this->message;
    }
    public function getCreated()
    {
        return $this->created;
    }

    public function isValid() : bool
    {
        if( mb_strlen( trim($this->username) ) < 3 )
        {
            $this->errors['username_field'] = "Le pseudo doit comporter au moins 3 caractères";
            return false;
        }
        if( mb_strlen( trim($this->message) ) < 10 )
        {
            $this->errors['message_field'] = "Le message doit comporter au moins 10 caractères";
            return false;
        }
        $this->errors['success'] = "Merci pour votre message";
        return true;
    }

    public function getErrors() : array
    {
        return $this->errors;
    }

    public function toHTML() : string
    {
        $html = "<p><strong>".$this->username."</strong> <em>il y a %s</em><br>". 
        $this->message."</p>";

        return $html;
    }

    public function toJSON() : string
    {
        return json_encode($this);
    }


}

