<?php
include('functions.php');
require "../Formulaire.php";
$form = new Formulaire('', 'POST');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Livre d'or</title>
</head>

<body>
    <div class="container">
        <div class="content col-md-8 offset-md-2">
            <?= errors_messages($errors, array_key_exists('success', $errors)) ?>
            <h3>Livre d'or</h3>
            <?php
            // PSEUDO
            $pseudo = $form->createInput(
                'pseudo',
                'text',
                [
                    'id' => 'pseudo',
                    'class' => 'form-control',
                    'placeholder' => "Votre pseudo",
                    'required' => 'required',
                    "value" => $pseudo ? $pseudo : "",
                ]
            );
            $div = $form->createDiv(['class' => "form-group"], $pseudo);
            $form->addChildren($form->getForm(), $div);

            // MESSAGE
            $message = $form->createTextarea(
                'message',
                [
                    'id' => 'message',
                    'class' => 'form-control',
                    'placeholder' => "Votre message",
                    'required' => 'required',
                    "rows" => 10,
                    "cols" => 30,
                    "value" => $message ? $message : "",
                ]
            );
            $div = $form->createDiv(['class' => "form-group"], $message);
            $form->addChildren($form->getForm(), $div);

            // SUBMIT BUTTON
            $submit = $form->createInput(
                'send',
                'submit',
                [
                    'class' => 'btn btn-primary',
                    "value" => "Envoyer"
                ]
            );
            $div = $form->createDiv(['class' => "form-group"], $submit);
            $form->addChildren($form->getForm(), $div);

            echo $form->getForm(); // display form
            ?>
            <?php if ($messages) : ?>
                <h4>Vos messages :</h4>

                <div class="messages jumbotron" style="padding: 1rem;">
                    <?php foreach (array_reverse($messages) as $message) : ?>
                        <?php printf($message->tohtml, humanTiming($message->created_at)) ?>
                    <?php endforeach ?>
                </div>
            <?php endif ?>
        </div>
    </div>
</body>

</html>