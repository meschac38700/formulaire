<?php
$messages = [];
$errors = [];
$pseudo=null;
$message=null;
require "Database.php";
$bdd = new Database(null);
if( isset($_POST['send']) )
{
    extract($_POST);
    require "Message.php";
    $msg = new Message(htmlspecialchars($pseudo), htmlspecialchars($message) );
    if( $msg->isValid() )
    {
        $bdd = new Database($msg);
        $bdd->saveMessage();
        
    }
    
    $errors = $msg->getErrors();
}
$messages = Database::getMessages();
if( array_key_exists("success", $errors) )
{
    $pseudo = "";
    $message = "";
}
require "views/index.view.php";

$errors['success'] ="";