<?php

class Formulaire
{
    /** 
     * The current form 
     * @var string
     */
    public $form;

    /**
     * @param string $p_action : form action url
     * @param string $p_method : form method GET/POST
     * @param array $p_options additional attributes [ facultative ]
     * @return void
     */
    public function __construct( string $p_action=null, string $p_method="GET",$p_options=[] )
    {
        $this->form  = "<form ";
        $this->form .= $p_action? "action='". $p_action ."' " : "";
        $this->form .= "method='". $p_method ."' ";
        if( $p_options )
        {
            $this->form = $this->addOptions( $this->form, $p_options );
        }
        $this->form .= ">";
    }


    public function createDiv( $p_attributes = null, $p_children=null )
    {
        $div = "<div ";
        if( $p_attributes )
        {
            $div = $this->addOptions($div, $p_attributes);
        }
        $div .= ">";
        if( $p_children )
        {
            $div = $this->addChildren( $div, $p_children );
            return $div;
        }
        $div .= "</div>";
        return $div;
    }

    /**
     * Add the label
     * @param array p_labelOptions : that contains this following keys
     *      @param string message : the message that will be display to the user
     *      @param string id : label's id
     *      @param string class : label's class
     * @param string p_name : that's hte input id, that permits to link a label to a input
     * @return string label tag
     */
    public function createLabel(string $p_message, array $p_labelOptions ) : string
    {
        $label  = "<label ";
        if( $p_labelOptions )
        {
            $label = $this->addOptions( $label, $p_labelOptions );
        }
        $label .= ">". $p_message ."</label>";
        return $label;
    }

    /**
     * Add the input
     * @param string p_name : the name of the input
     * @param string p_type : the type of the input
     * @param array p_options : others attributes examples : value, id , class, placeholder, required etc... [ facultative ]
     * @param array p_divSurroundAttributes : additional attributes to the div surround tag such as id, class etc...
     * @param array p_label : contains [message, id, class] [ facultative ]
     * @return void
     */
    public function createInput( string $p_name, string $p_type, array $p_options=[] ) : string
    {
        $input  = "<input ";
        $input .= "name='". $p_name ."' ";
        $input .= "type='". $p_type ."' ";
        
        if( $p_options )
        {
            $input = $this->addOptions($input, $p_options); // add some additional attributes
        }
        $input .= "/>";

        return $input;
    }

    /**
     * Add textarea tag
     * @param string p_name : the name of the textarea
     * @param array p_options : others attributes examples : value, id , class, placeholder, required etc... [ facultative ]
     * @param array p_divSurroundAttributes : additional attributes to the div surround tag such as id, class etc...
     * @param array p_label : contains [message, id, class] [ facultative ]
     */
    public function createTextarea( string $p_name, array $p_options=[] ) : string
    {
        $value ="";
        $textarea  = "<textarea ";
        $textarea .= "name='". $p_name ."' ";
        if( $p_options )
        {
            if( !empty($p_options['value']) )
            {
                $value = $p_options['value'];
                unset($p_options['value']);
            }
            $textarea = $this->addOptions($textarea, $p_options); // add some addtional attributes
        }
        $textarea .= ">". $value ."</textarea>";

        return $textarea;
    }
    
    /**
     * Deliver the form
     * @param void
     * @return string the form
     */
    public function getForm()
    {
        return $this->form;
    }

    // *************************************** OPTIIONS ***************************************

    /**
     * @param string parent : html tag
     * @param string or array html tags to put inside the parent
     */
    public function addChildren( string $parent, $children )
    {
        $tag_name = $this->getTagName( $parent );
        if( is_array( $children ) ) // if there are more than 1 child
        {
            foreach( $children as $child )
            {
                if( $this->parentIsAlreadyClosed($parent, $tag_name ) )
                {
                    $regex   = "%".preg_quote("</".$tag_name.">")."$%i";
                    $parent  = preg_replace($regex, '', $parent);
                    $parent .= $child;
                }
                else
                {
                    $parent .= $child;
                }
            }
        }
        else // if there one child
        {
            
            if( $this->parentIsAlreadyClosed($parent, $tag_name ) )
            {
                
                $regex   = "%".preg_quote("</".$tag_name.">")."$%i";
                $parent  = preg_replace($regex, '', $parent);
                $parent .= $children;
                
            }
            else
            {
                $parent .= $children;
            }
        }
        
        $parent .= "</".$tag_name.">";
        if( strtolower($tag_name) == "form" )
        {
            $this->form = $parent; return "";
        }
        return $parent;
    }

    /**
     * extract to tag his type ex <div></div> => div, <p></p> => p
     * @param string $html_tag 
     * @return string tagName 
     */
    private function getTagName( $html_tag ) : string
    {
        $tag_name = "";
        $regex = "%^<[a-z]+%i";
        $m = preg_match($regex, $html_tag, $matches);
        if( count($matches) >= 1 )
        {
            // replace < by ''
            $tag_name = preg_replace("%^<%i", "", $matches[0] );
        }
        return $tag_name ;
    }

    public static function parentIsAlreadyClosed( $parent, $tag_name )
    {
        
        $regex = "%<\/".$tag_name.">$%i";
        $match = preg_match( $regex, $parent);
        return $match;
    }

    /**
     * Surround some html tag with a div tag
     * @param array p_attributes somme attributes to add to the div tag
     * @param array p_tags html all tags to surround
     * @return string html tag DIV
     */
    private function surroundDiv( array $p_attributes, array $p_tags ) : string
    {
        $div  = "<div ";
        if( $p_attributes )
        {
            $div  = $this->addOptions($div, $p_attributes);
        }
        $div .= ">";
        foreach($p_tags as $tag)
        {
            $div .= $tag;
        }
        $div .= "</div>";
        
        return $div;
    }

    /**
     * Add some attributes to the tag
     * @param string p_tag
     * @param array p_options contains some additional attributes
     * @return string html tag
     */
    private function addOptions($p_tag, $p_options) : string
    {
        foreach( $p_options as $attributeName => $AttributeValue )
        {
            if( !is_int( $attributeName ) && !is_float( $attributeName ) ) // check if attributes name is not a number
            {
                $p_tag .= $attributeName . '= \''. $AttributeValue .'\' ';
            }
        }
        return $p_tag;
    }

}
