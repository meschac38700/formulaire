<?php
require "Formulaire.php";

class FormulaireBoostrap extends Formulaire
{

    public function __construct( string $p_action="#", string $p_method="GET",$p_options=[] )
    {
        parent::__construct( $p_action, $p_method, $p_options );
    }    

    public function createInput( string $p_name, string $p_type, array $p_options=[] ) : string
    {
        $input = parent::createInput( $p_name, $p_type, $p_options );
        if( strtolower($p_type) !== 'submit'  )
        {
            $label = parent::createLabel($p_name.' :', ['for'=>$p_name, 'class'=>'form-label']);
            $div = parent::createDiv(['class'=>'form-group'], [$label, $input]);
        }
        $div = parent::createDiv(['class'=>'form-group'], [$input]);
        return $div;
    }
    
    public function createTextarea( string $p_name, array $p_options=[] ) : string
    {
        $input = parent::createTextarea( $p_name, $p_options );
        $label = parent::createLabel($p_name.' :', ['for'=>$p_name, 'class'=>'form-label']);
        $div = parent::createDiv(['class'=>'form-group'], [$label, $input]);
        return $div;
    }
}